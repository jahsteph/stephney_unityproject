﻿using UnityEngine;

public class AddHealth : MonoBehaviour {

	PlayerHealth playerHealth;
	public int healthBonus = 50;
	public GameObject HealthPickUp;

	void Awake()
	{
		playerHealth = FindObjectOfType<PlayerHealth>();
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Player")) 
		{
		
		if (playerHealth.currentHealth < playerHealth.maxHealth) 
			{
				Destroy (gameObject);
				playerHealth.currentHealth = playerHealth.currentHealth + healthBonus;
			}
			if (other.CompareTag("HealthPickUp"))
			{
				playerHealth.healthSlider.value = playerHealth.currentHealth + healthBonus;
			}
		}
	}
}
