﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PillCount : MonoBehaviour {

	public static int pills;

	Text text;


	void Awake ()
	{
		text = GetComponent <Text> ();
		pills = 0;
	}


	void Update ()
	{
		text.text = "Pills: " + pills;
	}
}