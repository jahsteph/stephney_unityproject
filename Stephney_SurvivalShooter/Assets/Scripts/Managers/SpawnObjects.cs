﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObjects : MonoBehaviour {

	public GameObject HealthPickUp;
	public float Place1;
	public float Place2;

	void Start ()
	{
		Place1 = Random.Range (-10, 10);
		Place2 = Random.Range (-19, 19);
		HealthPickUp.transform.position = new Vector3 (Place1, 0, Place2);
	}
}
